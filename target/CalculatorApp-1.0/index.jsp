<html lang="en">
  <head>
    <meta charset="UTF-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge" />
    <meta name="viewport" content="width=b, initial-scale=1.0" />
    <link rel="stylesheet" href="./css/calculator.css" />
    <script src="/Js/app.js" defer></script>
    <title>Document</title>
  </head>
  <body>
    <form class="calculator">
  <div class="container">
        <input type="text" class="result">
        <table>
          <tr>
            <td><input type="button" value="7" class="global"></td>
            <td><input type="button" value="8" class="global"></td>
            <td><input type="button" value="9" class="global"></td>
            <td><input type="button" value="/" class="global"></td>
          </tr>
          <tr>
            <td><input type="button" value="4" class="global"></td>
            <td><input type="button" value="5" class="global"></td>
            <td><input type="button" value="6" class="global"></td>
            <td><input type="button" value="*" class="global"></td>
          </tr>
          <tr>
            <td><input type="button" value="1" class="global"></td>
            <td><input type="button" value="2" class="global"></td>
            <td><input type="button" value="3" class="global"></td>
            <td><input type="button" value="-" class="global"></td>
          </tr>
          <tr>
            <td colspan="2"><input type="button" value="0" class="global grey"></td>
            <td><input type="button" value="." class="global"></td>
            <td><input type="button" value="+" class="global"></td>
          </tr>
          <tr>
            <td><input type="button" value="AC" class="global red white-text"></td>
            <td colspan="3"><input type="button" value="=" class="global green white-text"></td>
          </tr>
        </table>
      </div>
    </form>
  </body>
</html>
